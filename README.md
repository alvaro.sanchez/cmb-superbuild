<p style="text-align:center"><img src="images/CMBLogoSuperBuild.png" alt="CMB SuperBuild Logo" align="middle" style="width: 200px;"/> </p>

# CMB SuperBuild

Though CMB is relatively simple to build itself, it does depend on a several
libraries including Qt, ParaView/VTK, MOAB, etc.. To help make the process of
building CMB and the libraries it depends on easier we have created a
SuperBuild CMake Project.

# Requirements

* CMake version 3.6 or greater with SSL support (Binaries from `cmake.org`
  have this already; custom built CMake binaries need to use
  `CMAKE_USE_SYSTEM_CURL=ON`).
* ninja or make - (the Windows build requires ninja)
* Checkout of the [CMB SuperBuild Git Repo](https://gitlab.kitware.com/cmb/cmb-superbuild)
* C++ Compiler
 * XCode 7.1 or greater - **Note if using XCode 8.x you will need the 10.11 SDK (there is an issue with QuickTime and OpenCV)**
 * GCC 4.8 or higher
 * Visual Studio 2013 64 bit
* Ubuntu 16.04 specific
 * sudo apt-get install m4
 * sudo apt-get install build-essential
 * turn on `use_SYSTEM_qt4` in CMake(Recommend)
  * sudo apt-get install qt4-qmake libqt4-dev qt4-dev-tools(if using system qt)
 * sudo apt-get install libxt-dev

Note that the build process will also download additional tarballs and
checkout additional git repos so you will also need an internet connection.

# Building CMB using the SuperBuild Process

## Prepping the Git Repo

1. Clone the CMB SuperBuild Repo using `git clone https://gitlab.kitware.com/cmb/cmb-superbuild.git`
2. Using a shell in the cloned repository, run `git submodule update --init`

## Configuring the Build Using CMake

There are two possible methods you can use: CMake GUI or the ccmake command line tool

### Using the CMake GUI

![](images/CmakeScreenShot.png)

1. Select the Source directory that contains the CMake SuperBuild Git Repo
2. Select the build directory to be used to hold the build.  Note that due to
   a bug in git this should not be under and git repository including the
   Source Directory.

### Using ccmake commandline tool

1. Make the directory you want to build in
2. cd into that directory
3. If you are building with ninja (as oppose to make) run
   `ccmake -G Ninja PathToYourSuperBuildRepo`, else omit the `-G Ninja`

### Configuring the CMB SuperBuild

* By default the build will be in Release Mode (not Debug) - this can be
  changed using the `CMAKE_BUILD_TYPE_cmb` variable. Similar variables exist
  for other projects depending on the setup including `paraview` and `smtk`.
* The process will also build Qt 4.8.6 by default.  If you already have a Qt
  installed (4.8.\*) then you can do the following:
 * Turn `USE_SYSTEM_qt4` on
 * Tell CMake to configure
 * Check to see if the `QT_QMAKE_EXECUTABLE` variable is set to the
   appropriate qmake - if is not then set it correctly
 * NOTE!  Qt built on Mac OSX 10.9 and greater needs several patches to work
   properly.  These patches are automatically applied when Qt is built using
   the superbuild and it is the recommended way to build Qt.  You will also
   need to make sure you have a 10.9 SDK on your OSX platform
 * On Windows, the directory to `qmake` must be in the `PATH` environment
   variable in order to build.
* Tell CMake to configure
* Tell CMake to generate

## Building the CMB SuperBuild

* cd into the build directory
* run make or ninja - depending on which build system you previously selected.

## Building a CMB Installable Package

* cd into the build directory
* run ctest -R cpack
