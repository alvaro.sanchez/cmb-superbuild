superbuild_set_revision(boost
  URL     "https://midas3.kitware.com/midas/download/bitstream/457867/boost_1_60_0.tar.bz2"
  URL_MD5 65a840e1a0b13a558ff19eeb2c4f0cbe)

# XXX: When updating this, update the version number in CMakeLists.txt as well.
# The current version of ParaView is post 5.4.0 RC2

set(paraview_revision "0545c6a8186e368dc57517a14fc90ed103c0c981")

if (USE_PARAVIEW_master)
  set(paraview_revision origin/master)
endif ()
superbuild_set_revision(paraview
  GIT_REPOSITORY "https://gitlab.kitware.com/paraview/paraview.git"
  GIT_TAG        "${paraview_revision}")

superbuild_set_revision(vtk
  GIT_REPOSITORY "https://gitlab.kitware.com/vtk/vtk.git"
  GIT_TAG        origin/master)

superbuild_set_revision(shiboken
  # https://github.com/OpenGeoscience/shiboken.git
  URL     "http://www.paraview.org/files/dependencies/shiboken-110e45fa9d873afea4d7ae47da3fb678b1831a21.tar.bz2"
  URL_MD5 53e71b32964b5daf38b45e1679623e48)

superbuild_set_external_source(smtk
  "https://gitlab.kitware.com/cmb/smtk.git" "origin/master"
  "/dev/null"
  "/dev/null")

superbuild_set_external_source(cmb
  "https://gitlab.kitware.com/cmb/cmb.git" "origin/master"
  "/dev/null"
  "/dev/null")

superbuild_set_revision(cmbworkflows
  GIT_REPOSITORY "https://gitlab.kitware.com/cmb/simulation-workflows.git"
  GIT_TAG        origin/master)

superbuild_set_revision(vxl
  # https://github.com/judajake/vxl.git
  URL     "http://www.paraview.org/files/dependencies/vxl-44433e4bd8ca3eabe4e5441444bf2a050d689d45.tar.bz2"
  URL_MD5 dfff6d1958334cbbadb4edbf8c3f4cb6)

# Use opencv from Thu Oct 6 13:40:33 2016 +0000
superbuild_set_revision(opencv
  # https://github.com/opencv/opencv.git
  URL     "http://www.computationalmodelbuilder.org/files/dependencies/opencv-dd379ec9fddc1a1886766cf85844a6e18d38c4f1.tar.bz2"
  URL_MD5 19bbd14ed1bd741beccd6d19e444552f)

# Use the tweaked cmake build of zeromq
superbuild_set_revision(zeromq
  # https://github.com/robertmaynard/zeromq4-x.git
  URL     "http://www.computationalmodelbuilder.org/files/dependencies/zeromq4-6d787cf69da6c69550e85a45be1bee1eb0e1c415.tar.bz2"
  URL_MD5 26790786e01a1732a8acf723b99712ba)

# Use remus from Fri Jul 29 15:25:33 2016 -0400
superbuild_set_revision(remus
  GIT_REPOSITORY "https://gitlab.kitware.com/cmb/remus.git"
  GIT_TAG        1148df5bb86a2dc9bd61d19edbe6f77c582a5641)

superbuild_set_revision(gdal
  # https://github.com/judajake/gdal-svn.git
  URL     "http://www.computationalmodelbuilder.org/files/dependencies/gdal-98353693d6f1d607954220b2f8b040375e3d1744.tar.bz2"
  URL_MD5 5aa285dcc856f98ce44020ae1ae192cb)

superbuild_set_revision(moab
  # https://gitlab.kitware.com/third-party/moab.git
  URL     "http://www.computationalmodelbuilder.org/files/dependencies/moab-17bf5518c4e6585a824edf06bc31902baf9ad045.tar.bz2"
  URL_MD5 c1dd28514fc567b6e42acc12c2381ca4)

superbuild_set_revision(triangle
  # https://github.com/robertmaynard/triangle.git
  URL     "http://www.paraview.org/files/dependencies/triangle-4c20820448cdfa27f968cfd7cb33ea5b9426ad91.tar.bz2"
  URL_MD5 9a016bc90f1cdff441c75ceb53741b11)

superbuild_set_revision(pythonsix
  URL     "https://pypi.python.org/packages/source/s/six/six-1.10.0.tar.gz"
  URL_MD5 34eed507548117b2ab523ab14b2f8b55)

superbuild_set_revision(pythonrequests
  URL     "https://pypi.python.org/packages/source/r/requests/requests-2.9.1.tar.gz"
  URL_MD5 0b7f480d19012ec52bab78292efd976d)

superbuild_set_revision(pythonsetuptools
  URL     "https://pypi.python.org/packages/45/5e/79ca67a0d6f2f42bfdd9e467ef97398d6ad87ee2fa9c8cdf7caf3ddcab1e/setuptools-23.0.0.tar.gz"
  URL_MD5 100a90664040f8ff232fbac02a4c5652)

superbuild_set_revision(pythongirderclient
  URL     "https://pypi.python.org/packages/source/g/girder-client/girder-client-1.1.2.tar.gz"
  URL_MD5 4cd5e0cab41337a41f45453d25193dcf)

superbuild_set_revision(pybind11
#  GIT_REPOSITORY "https://github.com/pybind/pybind11.git"
#  GIT_TAG        05920e363edfd5d8dc6b4608820d749af5217b5f)
  URL     "http://www.computationalmodelbuilder.org/files/dependencies/pybind11-05920e363edfd5d8dc6b4608820d749af5217b5f.tar.bz2"
  URL_MD5 48435b9c662dba020e1db0a6401d189b)

superbuild_set_revision(ftgl
  # https://github.com/ulrichard/ftgl.git
  URL     "http://www.paraview.org/files/dependencies/ftgl-dfd7c9f0dee7f0059d5784f3a71118ae5c0afff4.tar.bz2"
  URL_MD5 16e54c7391f449c942f3f12378db238f)

superbuild_set_revision(oce
  # https://github.com/mathstuf/oce.git
  URL     "http://www.paraview.org/files/dependencies/oce-9b4646a11c7d9be65a6c2df0ade6604cc91b1fa4.tar.bz2"
  URL_MD5 adea5bd5a7510c7da58755ca7d964319)

superbuild_set_revision(cgm
  # https://bitbucket.org/mathstuf/cgm.git
  URL     "http://www.computationalmodelbuilder.org/files/dependencies/cgm-3191a7219e16491ebe098159598ddfe7f039bffc.tar.bz2"
  URL_MD5 bf280f3bc5970c98960da9a66da2ab94)

superbuild_set_revision(paraviewwebvisualizer
  URL     "http://www.paraview.org/files/dependencies/visualizer-2.0.12.tar.gz"
  URL_MD5 56e7e241ea6ad66b44469fc3186f47d6)

superbuild_set_revision(paraviewweblightviz
  URL     "http://www.paraview.org/files/dependencies/light-viz-1.16.1.tar.gz"
  URL_MD5 9ac1937cf07ae57bf85c3240f921679a)

superbuild_set_revision(cmbusersguide
  URL     "https://media.readthedocs.org/pdf/cmb/master/cmb.pdf")

superbuild_set_revision(smtkusersguide
  URL     "https://media.readthedocs.org/pdf/smtk/latest/smtk.pdf")
