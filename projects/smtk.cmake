set(smtk_extra_cmake_args)
if (WIN32)
  # On Windows we expect the Python source for module to be
  # in a different place than Unix builds and in a different
  # place than SMTK would put it by default. Tell SMTK where
  # to install Python source for the smtk module:
  list(APPEND smtk_extra_cmake_args
    "-DSMTK_PYTHON_MODULEDIR:PATH=bin/Lib/site-packages")
endif ()

set(smtk_enable_python_wrapping)
if (shiboken_enabled OR pybind11_enabled)
  set(smtk_enable_python_wrapping ON)
endif ()

set(smtk_enable_vtk OFF)
if (vtk_enabled OR paraview_enabled)
  set(smtk_enable_vtk ON)
endif ()

#explicitly depend on gdal so we inherit the location of the GDAL library
#which FindGDAL.cmake fails to find, even when given GDAL_DIR.
superbuild_add_project(smtk
  DEVELOPER_MODE
  DEBUGGABLE
  DEFAULT_ON
  DEPENDS boost cxx11 hdf5 netcdf
  DEPENDS_OPTIONAL cgm cumulus gdal moab netcdf opencv paraview pybind11 python
                   remus shiboken qt qt4 qt5 vtk
  CMAKE_ARGS
    ${smtk_extra_cmake_args}
    -DBUILD_SHARED_LIBS:BOOL=ON
    -DSMTK_ENABLE_OPENCV:BOOL=${opencv_enabled}
    -DSMTK_ENABLE_QT_SUPPORT:BOOL=${qt_enabled}
    -DSMTK_ENABLE_VTK_SUPPORT:BOOL=${smtk_enable_vtk}
    -DSMTK_ENABLE_PARAVIEW_SUPPORT:BOOL=${paraview_enabled}
    -DSMTK_ENABLE_CGM_SESSION:BOOL=${cgm_enabled}
    -DSMTK_ENABLE_DISCRETE_SESSION:BOOL=${smtk_enable_vtk}
    -DSMTK_ENABLE_EXODUS_SESSION:BOOL=${paraview_enabled}
    -DSMTK_ENABLE_REMOTE_SESSION:BOOL=${remus_enabled}
    -DSMTK_ENABLE_REMUS_SUPPORT:BOOL=${remus_enabled}
    -DSMTK_ENABLE_PYTHON_WRAPPING:BOOL=${smtk_enable_python_wrapping}
    -DSMTK_USE_PYBIND11:BOOL=${pybind11_enabled}
    -DPYBIND11_INSTALL:BOOL=${pybind11_enabled}
    -DSMTK_QT_VERSION:STRING=${qt_version}
    -DSMTK_DATA_DIR:PATH=${CMB_TEST_DATA_ROOT}

    # This should be off by default because vtkCmbMoabReader in discrete
    # session may only be needed for debugging purpose
    -DSMTK_ENABLE_MOAB_DISCRETE_READER:BOOL=OFF

    -DSMTK_USE_SYSTEM_MOAB:BOOL=${moab_enabled}
    -DMOAB_INCLUDE_DIR:PATH=<INSTALL_DIR>/include

    # MOAB bits
    -DENABLE_HDF5:BOOL=${hdf5_enabled}
    -DENABLE_NETCDF:BOOL=${netcdf_enabled}
    -DNETCDF_ROOT:PATH=<INSTALL_DIR>

    # GDAL bits to properly enable gdal classes ( mainly wrapper )
    # that we need to build
    -DGDAL_DIR:PATH=<INSTALL_DIR>

    # Cumulus bits
    -DSMTK_ENABLE_CUMULUS_SUPPORT:BOOL=${cumulus_enabled}

    -DSMTK_FIND_SHIBOKEN:STRING=

    # Set CMAKE_INSTALL_LIBDIR to "lib" for all projects in the superbuild to
    # override OS-specific libdirs that GNUInstallDirs.cmake would otherwise
    # set.
    -DCMAKE_INSTALL_LIBDIR:STRING=lib)

if (WIN32)
  set(smtk_cmakedir bin/cmake)
else ()
  set(smtk_cmakedir lib/cmake)
endif ()

superbuild_add_extra_cmake_args(
  -DSMTK_DIR:PATH=<INSTALL_DIR>/${smtk_cmakedir}/SMTK)

if ((CMAKE_CXX_COMPILER_ID STREQUAL "AppleClang" AND NOT CMAKE_CXX_COMPILER_VERSION VERSION_LESS "7.0") OR
    (CMAKE_CXX_COMPILER_ID STREQUAL "Clang" AND NOT CMAKE_CXX_COMPILER_VERSION VERSION_LESS "3.5"))
 superbuild_append_flags(cxx_flags "-Wno-inconsistent-missing-override" PROJECT_ONLY)
endif ()
