set(cmb_package "ModelBuilder ${cmb_version_major}.${cmb_version_minor}.${cmb_version_patch}")
set(cmb_doc_dir "${cmb_package}/Contents/doc")

include(modelbuilder.bundle.common)
include(cmb.bundle.apple)

# Install PDF guides.
cmb_install_extra_data()
